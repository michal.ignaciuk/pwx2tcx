package org.mignaciuk.pwx2tcx.wrappers;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.mignaciuk.pwx2tcx.bindings.pwx.MinMaxAvg;
import org.mignaciuk.pwx2tcx.bindings.pwx.Pwx.Workout;
import org.mignaciuk.pwx2tcx.bindings.pwx.Pwx.Workout.Sample;
import org.mignaciuk.pwx2tcx.bindings.pwx.SportTypes;

public class PwxWorkoutSummaryWrapper {

	private Workout workout;

	public PwxWorkoutSummaryWrapper(Workout workout) {
		this.workout = workout;
	}

	public SportTypes getSportType() {
		return workout.getSportType();
	}

	public XMLGregorianCalendar getTime() {
		return workout.getTime();
	}

	public List<Sample> getSamples() {
		return workout.getSample();
	}

	public MinMaxAvg getHrSummary() {
		return workout.getSummarydata().getHr();
	}

	public int getTotalCalories() {
		//1cal=4.184kJ
		if (workout.getSummarydata().getWork() == null) {
			return 0;
		}
		Double kJ = workout.getSummarydata().getWork();
		return (int)(kJ.doubleValue()/4.184);
	}

	public double getTotalDistance() {
		return workout.getSummarydata().getDist() != null ? workout.getSummarydata().getDist() : 0;
	}

	public MinMaxAvg getSpeedSummary() {
		return workout.getSummarydata().getSpd();
	}

	public XMLGregorianCalendar getStartTime() {
		return workout.getTime();
	}

	public double getTotalTimeSeconds() {
		return workout.getSummarydata().getDuration();
	}

}
