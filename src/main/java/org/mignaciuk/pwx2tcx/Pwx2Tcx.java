package org.mignaciuk.pwx2tcx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.mignaciuk.pwx2tcx.converters.ConverterHelper;

public class Pwx2Tcx {

	public void watch(String pathName) throws IOException, InterruptedException {
		Path toWatch = Paths.get(pathName);
		WatchService watcherSvc = FileSystems.getDefault().newWatchService();
		WatchKey watchKey = toWatch.register(watcherSvc, StandardWatchEventKinds.ENTRY_CREATE);

		while (true) {
			try {
				watchKey = watcherSvc.take();
				Thread.sleep(1000);
				for (WatchEvent<?> event : watchKey.pollEvents()) {
					@SuppressWarnings("unchecked")
					WatchEvent<Path> watchEvent = (WatchEvent<Path>) event;
					File inputFile = toWatch.resolve(watchEvent.context()).toFile();
					FileInputStream inputStream = new FileInputStream(inputFile);
					if (inputFile.getAbsolutePath().endsWith("pwx")) {
						String outputFileName = inputFile.getAbsolutePath().substring(0,
								inputFile.getAbsolutePath().length() - 3)
								+ "tcx";

						File of = new File(outputFileName);
						FileOutputStream outputStream = new FileOutputStream(of);
						try {
							System.out.println("Conversion of file " + inputFile.getAbsolutePath() + " started.");
							ConverterHelper.transform(inputStream, outputStream);
							System.out.println("File " + inputFile.getAbsolutePath() + " saved as " + outputFileName);
						} catch (JAXBException e) {
							e.printStackTrace();
						} catch (ParserConfigurationException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							if (inputStream != null) {
								inputStream.close();
							}
							if (outputStream != null) {
								outputStream.flush();
								outputStream.close(); // doesn't close...
							}
						}
					}
					watchKey.reset();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

}
