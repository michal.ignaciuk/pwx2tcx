package org.mignaciuk.pwx2tcx.converters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;

import org.mignaciuk.pwx2tcx.bindings.pwx.MinMaxAvg;
import org.mignaciuk.pwx2tcx.bindings.pwx.Pwx;
import org.mignaciuk.pwx2tcx.bindings.pwx.Pwx.Workout.Sample;
import org.mignaciuk.pwx2tcx.bindings.pwx.SportTypes;
import org.mignaciuk.pwx2tcx.bindings.tcx.ActivityLapT;
import org.mignaciuk.pwx2tcx.bindings.tcx.ActivityListT;
import org.mignaciuk.pwx2tcx.bindings.tcx.ActivityT;
import org.mignaciuk.pwx2tcx.bindings.tcx.HeartRateInBeatsPerMinuteT;
import org.mignaciuk.pwx2tcx.bindings.tcx.IntensityT;
import org.mignaciuk.pwx2tcx.bindings.tcx.ObjectFactory;
import org.mignaciuk.pwx2tcx.bindings.tcx.PositionT;
import org.mignaciuk.pwx2tcx.bindings.tcx.SportT;
import org.mignaciuk.pwx2tcx.bindings.tcx.TrackpointT;
import org.mignaciuk.pwx2tcx.bindings.tcx.TrainingCenterDatabaseT;
import org.mignaciuk.pwx2tcx.wrappers.PwxWorkoutSummaryWrapper;
import org.xml.sax.SAXException;

public class ConverterHelper {

	private static final ObjectFactory factory = new ObjectFactory();

	public static void transform(InputStream inputStream, OutputStream outputStream) throws JAXBException,
			ParserConfigurationException, SAXException, IOException {
		JAXBContext pwxContext = JAXBContext.newInstance(Pwx.class);
		Unmarshaller pwxUnmarshaller = pwxContext.createUnmarshaller();
		Pwx pwx = (Pwx) pwxUnmarshaller.unmarshal(inputStream);

		int workoutNumber = 0; // TODO - multiple workouts in one file
		if (pwx.getWorkout().size() > 1) {
			System.err
					.println("Multiple Workouts found in the input file. Currently only single workout files are supported.");
			System.exit(20);
		}
		TrainingCenterDatabaseT tcx = ConverterHelper.convert(new PwxWorkoutSummaryWrapper(pwx.getWorkout().get(
				workoutNumber)));
		JAXBContext tcxContext = JAXBContext.newInstance(TrainingCenterDatabaseT.class);
		ObjectFactory factory = new ObjectFactory();
		JAXBElement<TrainingCenterDatabaseT> createTrainingCenterDatabase = factory.createTrainingCenterDatabase(tcx);
		Marshaller tcxMarshaller = tcxContext.createMarshaller();
		tcxMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		tcxMarshaller.marshal(createTrainingCenterDatabase, outputStream);
	}

	private static TrainingCenterDatabaseT convert(PwxWorkoutSummaryWrapper pwx) {
		ObjectFactory factory = new ObjectFactory();

		TrainingCenterDatabaseT tcx = factory.createTrainingCenterDatabaseT();
		ActivityListT activityListT = factory.createActivityListT();
		tcx.setActivities(activityListT);
		ActivityT activity = factory.createActivityT();
		activity.setSport(ConverterHelper.convert(pwx.getSportType()));
		activity.setId(pwx.getTime());
		activityListT.getActivity().add(activity);

		List<Sample> samples = pwx.getSamples();
		ActivityLapT lap = ConverterHelper.convertToLap(pwx);
		activity.getLap().add(lap);

		lap.getTrack().add(factory.createTrackT());
		List<TrackpointT> trackpoints = lap.getTrack().get(0).getTrackpoint();

		for (Sample sample : samples) {
			TrackpointT trackpoint = ConverterHelper.convertToTrackpoint(sample, pwx.getTime());
			trackpoints.add(trackpoint);
		}

		return tcx;
	}

	private static SportT convert(SportTypes sportType) {
		switch (sportType) {
			case BIKE:
				return SportT.BIKING;
			case RUN:
				return SportT.RUNNING;
			default:
				return SportT.OTHER;
		}
	}

	private static HeartRateInBeatsPerMinuteT convertHr(Short hr) {
		HeartRateInBeatsPerMinuteT heartRateInBeatsPerMinuteT = factory.createHeartRateInBeatsPerMinuteT();
		heartRateInBeatsPerMinuteT.setValue(hr);
		return heartRateInBeatsPerMinuteT;
	}

	private static PositionT convertPosition(Double lon, Double lat) {
		if (lon == null || lat == null) {
			return null;
		}
		PositionT position = factory.createPositionT();
		position.setLongitudeDegrees(lon);
		position.setLatitudeDegrees(lat);
		return position;
	}

	private static HeartRateInBeatsPerMinuteT convertToAvgHr(Double avg) {
		HeartRateInBeatsPerMinuteT heartRateInBeatsPerMinuteT = factory.createHeartRateInBeatsPerMinuteT();
		heartRateInBeatsPerMinuteT.setValue(avg.shortValue());
		return heartRateInBeatsPerMinuteT;
	}

	private static HeartRateInBeatsPerMinuteT convertToMaxHr(Double max) {
		HeartRateInBeatsPerMinuteT heartRateInBeatsPerMinuteT = factory.createHeartRateInBeatsPerMinuteT();
		heartRateInBeatsPerMinuteT.setValue(max.shortValue());
		return heartRateInBeatsPerMinuteT;
	}

	private static Double convertToMaxSpeed(MinMaxAvg speedSummary) {
		return speedSummary.getMax();
	}

	private static TrackpointT convertToTrackpoint(Sample sample, XMLGregorianCalendar startTime) {
		TrackpointT trackpoint = factory.createTrackpointT();
		trackpoint.setAltitudeMeters(sample.getAlt());
		trackpoint.setCadence(sample.getCad());
		trackpoint.setDistanceMeters(sample.getDist());
		if (sample.getHr() != null) {
			trackpoint.setHeartRateBpm(ConverterHelper.convertHr(sample.getHr()));
		}
		trackpoint.setPosition(ConverterHelper.convertPosition(sample.getLon(), sample.getLat()));
		// trackpoint.setSensorState(?);
		try {
			XMLGregorianCalendar time = ((XMLGregorianCalendar) startTime.clone());

			time.add(DatatypeFactory.newInstance().newDuration((long) (sample.getTimeoffset() * 1000)));
//			int tzoffset = TimeZone.getDefault().getOffset(System.currentTimeMillis())/1000/60;
//			time.setTimezone(tzoffset);

			trackpoint.setTime(time);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return trackpoint;
	}

	private static ActivityLapT convertToLap(PwxWorkoutSummaryWrapper pwx) {
		ActivityLapT lap = factory.createActivityLapT();
		if (pwx.getHrSummary() != null) {
			if (pwx.getHrSummary().getAvg() != null) {
				lap.setAverageHeartRateBpm(ConverterHelper.convertToAvgHr(pwx.getHrSummary().getAvg()));
			}
			if (pwx.getHrSummary().getMax() != null) {
				lap.setMaximumHeartRateBpm(ConverterHelper.convertToMaxHr(pwx.getHrSummary().getMax()));
			}
		}
		// lap.setCadence(?);
		lap.setCalories(pwx.getTotalCalories());
		lap.setDistanceMeters(pwx.getTotalDistance());
		lap.setIntensity(IntensityT.ACTIVE);// ?
		if (pwx.getSpeedSummary() != null) {
			lap.setMaximumSpeed(ConverterHelper.convertToMaxSpeed(pwx.getSpeedSummary()));
		}
		lap.setNotes("Converted wiht pwx2tcx.");
		lap.setStartTime(pwx.getStartTime());
		lap.setTotalTimeSeconds(pwx.getTotalTimeSeconds());
		return lap;
	}

}