package org.mignaciuk.pwx2tcx;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * Enterprise Application Client main class.
 *
 */
public class Main {

	private static Map<PreferenceName, String> properties = new HashMap<PreferenceName, String>();

	public static void main(String[] args) throws JAXBException, ParserConfigurationException, SAXException,
			IOException, InterruptedException {
		if (args.length != 1) {
			System.out.println("Usage: java -jar pwx2tcx-{version}.jar {pwx_folder_to_watch}\ni.e.java -jar pwx2tcx-1.0.jar \"C:\\Users\\sportsman\\Documents\\TrainingPeaks\\Device Agent\\saved\\sportsman\\\"");
			return;
		}
		properties.put(PreferenceName.DIRECTORY_TO_WATCH, args[0]);
		Pwx2Tcx pwxtcx = new Pwx2Tcx();
		pwxtcx.watch(properties.get(PreferenceName.DIRECTORY_TO_WATCH));
	}

}
