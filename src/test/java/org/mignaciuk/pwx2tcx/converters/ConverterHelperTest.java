package org.mignaciuk.pwx2tcx.converters;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

public class ConverterHelperTest {

	@Test
	public void testTransform() throws JAXBException, ParserConfigurationException, SAXException, IOException {
		InputStream is = this.getClass().getClassLoader()
				.getResourceAsStream("xml\\pwx\\sportsman_Timex_Global_Trainer_2013_06_15_12_01_06.pwx");
		ByteArrayOutputStream result = new ByteArrayOutputStream();

		ConverterHelper.transform(is, result);

		InputStream isTarget = this.getClass().getClassLoader()
				.getResourceAsStream("xml\\tcx\\sportsman_Timex_Global_Trainer_2013_06_15_12_01_06.tcx");
		String content = CharStreams.toString(new InputStreamReader(isTarget, Charsets.UTF_8));
		Assert.assertEquals(result.toString("UTF-8"), content);
	}
	
	@Test
	public void testTransform2() throws JAXBException, ParserConfigurationException, SAXException, IOException {
		InputStream is = this.getClass().getClassLoader()
				.getResourceAsStream("xml\\pwx\\sportsman_Timex_Global_Trainer_2013_07_06_19_17_24.pwx");
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		ConverterHelper.transform(is, result);
	}

}
